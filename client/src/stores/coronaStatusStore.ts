import { makeAutoObservable } from "mobx"
import CoronaStatus from "../models/coronaStatus"
import coronaAPI from "../apis/coronaAPI"


export class CoronaStatusStore {
    
    coronaStatus?: CoronaStatus

    constructor() {
        makeAutoObservable(this)
    }

    setCoronaStatus = (coronaStatus?: CoronaStatus) => {
        this.coronaStatus = coronaStatus
    }

    fetchCoronaStatus = async (code: string) => {
        const coronaStatus = await coronaAPI.getCoronaStatus(code)
        this.setCoronaStatus(coronaStatus)
    }
  
}

const coronaStatusStore = new CoronaStatusStore()

export default coronaStatusStore