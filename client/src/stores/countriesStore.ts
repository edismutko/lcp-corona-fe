import { makeAutoObservable  } from "mobx"
import Country from "../models/country"
import countriesAPI from "../apis/countriesAPI"
import { makePersistable } from 'mobx-persist-store';

export class CountriesStore {
    
    countries?: Country[]
    selectedCountryIndex: number = 0

    constructor(){
        makeAutoObservable(this);
        makePersistable(this,{name:'CountriesStore',properties:['selectedCountryIndex'], storage:window.localStorage})
        this.fetchCountries();
    }

    setSelectedCountryIndex=(index:number)=>{
        this.selectedCountryIndex = index
    }

    setCountries = (countries?: Country[]) => {
        this.countries = countries
    }

    fetchCountries = async () =>{
        const countries = await countriesAPI.getCountries()
        this.setCountries(countries)
    }
    
    get selectedCountry() { 
        if (this.countries)
            return this.countries[this.selectedCountryIndex]
        return undefined
    }
}


const countriesStore = new CountriesStore()

export default countriesStore