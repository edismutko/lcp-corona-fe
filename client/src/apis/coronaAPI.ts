import CoronaStatus from "../models/coronaStatus"

const coronaAPI = {
    async getCoronaStatus(countryCode: string): Promise<CoronaStatus>{
        var result = await fetch(`https://localhost:44397/api/v1/coronaStatus/${countryCode}`,
        {
            method:'GET',
            headers:{
                'Content-Type': 'application/json',
            }
        })
        var body = await result.json()
        var coronaStatus = body as CoronaStatus
        return coronaStatus
    }
}
export default coronaAPI;