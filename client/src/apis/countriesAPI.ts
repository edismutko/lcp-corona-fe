import Country from '../models/country'

const countriesAPI = {
    async getCountries(): Promise<Country[]> {
        var result = await fetch('https://localhost:44397/api/v1/countries',
            { 
                method:'GET',  
                headers:{
                    'Content-Type': 'application/json',
            }})
        var body = await result.json()
        var countries = body as Country[]
        return countries;
    }
}

export default countriesAPI



