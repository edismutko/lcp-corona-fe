
export default interface Country{
    name: string
    code: string
    capital: string
    flag: string
}