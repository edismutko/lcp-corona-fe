
export default interface CoronaStatus{
    deaths:number,
    infections:number,
    recovered:number,
    date:Date,
    growthPercentages:number,
    IsPresentLess:boolean
}