import CoronaStatus from "./coronaStatus"
import Box from '@mui/material/Box'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import { CircularProgress, Paper, Typography } from "@mui/material"
import { makeStyles } from "@mui/styles"
import { CountriesStore } from "../stores/countriesStore"
import { observer } from "mobx-react"
import coronaStatusStore from "../stores/coronaStatusStore"


const useStyles = makeStyles({
    rootContainer: {
        paddingLeft: '25%',
        paddingRight: '25%',
    },
    countrySelector: {
        width: 'calc(100% - 40px)',
        margin: '20px'
    },
    countryFlagContainer: {
        padding: '20px',
        margin: '20px'
    },
    coronaStatusContainer: {
        padding: '20px',
        margin: '20px'
    },
    progress: {
        zIndex: 1005,
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        opacity: 0.5,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'red'
    },
    imgFlag: {
        margin: 'auto',
        marginTop: '20px',
    }
})

export interface CountriesProps {
    store: CountriesStore
}

const Countries = observer(({ store }: CountriesProps) => {
    const { countries } = store
    const { selectedCountryIndex } = store
    const { selectedCountry } = store

    const classes = useStyles()

    const onChangeCountry = (event: any) => {
        store.setSelectedCountryIndex(event.target.value)
    }

    return (
        <div>
            {(countries === undefined) ? (
                <div className={classes.progress}>
                    <CircularProgress color='inherit' />
                </div>) :
                (<Box width={1} className={classes.rootContainer}>
                    <Box width={0.5}>
                        <Select className={classes.countrySelector}
                            onChange={onChangeCountry}
                            value={selectedCountryIndex}
                        >
                            {
                                countries.map((item, index) => {
                                    return <MenuItem key={item.code} value={index}>{item.name}</MenuItem>
                                })
                            }

                        </Select>
                        {(selectedCountry === undefined) ? (
                            <div className={classes.progress}>
                                <CircularProgress color='inherit' />
                            </div>) :
                            (<>
                                <Paper className={classes.countryFlagContainer}>
                                    <Typography variant="h5" component="div">
                                        {selectedCountry.capital}
                                    </Typography>
                                    <img alt="flag" src={selectedCountry.flag} className={classes.imgFlag} />
                                </Paper>

                                <Paper className={classes.coronaStatusContainer}>
                                    <CoronaStatus country={selectedCountry} store={coronaStatusStore} />
                                </Paper>
                            </>)}

                    </Box>
                </Box>)
            }

        </div>
    );
})

export default Countries



