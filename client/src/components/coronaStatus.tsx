import { Box, CircularProgress, Typography } from "@mui/material"
import { makeStyles } from "@mui/styles"
import { observer } from "mobx-react-lite"
import { useEffect } from "react"
import Country from "../models/country"
import { CoronaStatusStore } from "../stores/coronaStatusStore"

interface CoronaStatusProps {
    country: Country,
    store: CoronaStatusStore
}

const useStyles = makeStyles({
    pogress: {        
        opacity: 0.5,
        color:'red'
    },
    typographyStatus:{
        fontWeight:'bold'
    },
    dailyGrowthPercent:{
        textAlign:'end'
    },
    spanNumber:{
        fontWeight:'normal'
    }
})

const CoronaStatus = (props: CoronaStatusProps) => {
    const {country} = props
    const {store} = props
    const {coronaStatus} = store
    const classes = useStyles()

    useEffect(() => {
        store.setCoronaStatus(undefined)
        store.fetchCoronaStatus(country.code)
    }, [country, store])

    
    return (
        <Box >
            { coronaStatus === undefined ?
                <div >
                    <CircularProgress color='inherit' className={classes.pogress}/>
                </div>
                :
                <>
                    <Box>
                        <Typography component="div" >
                            Getting less daily infections
                            {coronaStatus.IsPresentLess ?
                                <Typography style={{ color: "green" }}>V</Typography> :
                                <Typography style={{ color: "red" }}>X</Typography>
                            }
                        </Typography>
                        <Typography variant='h5'  className={classes.typographyStatus} >
                            Deaths:<span className={classes.spanNumber}>{coronaStatus.deaths}</span>
                        </Typography>
                        <Typography variant='h5' className={classes.typographyStatus}>
                            Infections:<span className={classes.spanNumber}>{coronaStatus.infections}</span>
                        </Typography>
                        <Typography variant='h5' className={classes.typographyStatus}>
                            Recovered:<span className={classes.spanNumber}>{coronaStatus.recovered}</span>
                        </Typography>
                        <Typography className={classes.dailyGrowthPercent}>
                            Latest daily growth: { coronaStatus.growthPercentages.toFixed(2) }%
                        </Typography>
                        
                    </Box>                   
                </>
            }
        </Box >
    );
}

export default observer(CoronaStatus)