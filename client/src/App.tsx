import './App.css'
import Countries from './components/countries'
import countriesStore from './stores/countriesStore';

const App = () => {
  return (
    <div className="App">     
      <Countries store={countriesStore}/>
    </div>
  );
}

export default App;
